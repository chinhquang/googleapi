//
//  ViewController.swift
//  UBERFLASH
//
//  Created by Night Dog on 10/27/18.
//  Copyright © 2018 NIGHTDOG. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import GoogleSignIn
import Google
class ViewController: UIViewController, UISearchBarDelegate, GIDSignInUIDelegate{
    var googleMapView : GMSMapView!
    
    
    @IBAction func searchWithAddress(_ sender: Any) {
        let searchControl = UISearchController(searchResultsController: nil)
        searchControl.searchBar.delegate = self
        self.present(searchControl, animated: true,completion: nil)
    }
    @IBOutlet weak var googleMapContainer: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super .viewDidAppear(true)
        self.googleMapView = GMSMapView (frame: self.googleMapContainer.frame)
        self.view.addSubview(googleMapView)
        
        var error : NSError?
        GGLContext.sharedInstance()?.configureWithError(&error)
        if error != nil {
            print(error as Any)
            
        }
        GIDSignIn.sharedInstance()?.uiDelegate = self
        let signInButton = GIDSignInButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50));
        signInButton.center = view.center
        view.addSubview(signInButton)
    }
    
}

